/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | www.openfoam.com
     \\/     M anipulation  |
-------------------------------------------------------------------------------
    Copyright (C) 2019-2021 OpenCFD Ltd.
    Copyright (C) YEAR AUTHOR, AFFILIATION
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "functionObjectTemplate.H"
#define namespaceFoam  // Suppress <using namespace Foam;>
#include "fvCFD.H"
#include "unitConversion.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

defineTypeNameAndDebug(totalMassFunctionObject, 0);

addRemovableToRunTimeSelectionTable
(
    functionObject,
    totalMassFunctionObject,
    dictionary
);


// * * * * * * * * * * * * * * * Global Functions  * * * * * * * * * * * * * //

// dynamicCode:
// SHA1 = 571f886969a260a76018feab9f8995c415c6249a
//
// unique function name that can be checked if the correct library version
// has been loaded
extern "C" void totalMass_571f886969a260a76018feab9f8995c415c6249a(bool load)
{
    if (load)
    {
        // Code that can be explicitly executed after loading
    }
    else
    {
        // Code that can be explicitly executed before unloading
    }
}


// * * * * * * * * * * * * * * * Local Functions * * * * * * * * * * * * * * //

//{{{ begin localCode

//}}} end localCode

} // End namespace Foam


// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

const Foam::fvMesh&
Foam::totalMassFunctionObject::mesh() const
{
    return refCast<const fvMesh>(obr_);
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::
totalMassFunctionObject::
totalMassFunctionObject
(
    const word& name,
    const Time& runTime,
    const dictionary& dict
)
:
    functionObjects::regionFunctionObject(name, runTime, dict)
{
    read(dict);
}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::
totalMassFunctionObject::
~totalMassFunctionObject()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

bool
Foam::
totalMassFunctionObject::read(const dictionary& dict)
{
    if (false)
    {
        printMessage("read totalMass");
    }

//{{{ begin code
    #line 85 "/home/leon/OpenFOAM/leon-v2212/run/customBoussinesq/fracture_10cm/system/controlDict.functions.totalMass"
outputFilePtr.reset(new OFstream("totalMass.txt"));
        outputFilePtr() <<   "# Time    gSum(xCO2)"   << endl;
//}}} end code

    return true;
}


bool
Foam::
totalMassFunctionObject::execute()
{
    if (false)
    {
        printMessage("execute totalMass");
    }

//{{{ begin code
    #line 91 "/home/leon/OpenFOAM/leon-v2212/run/customBoussinesq/fracture_10cm/system/controlDict.functions.totalMass"
const volScalarField& xCO2 = mesh().lookupObject<volScalarField>("xCO2");

    outputFilePtr() << mesh().time().timeName() << "    "
            << gSum(xCO2) << endl;
//}}} end code

    return true;
}


bool
Foam::
totalMassFunctionObject::write()
{
    if (false)
    {
        printMessage("write totalMass");
    }

//{{{ begin code
    
//}}} end code

    return true;
}


bool
Foam::
totalMassFunctionObject::end()
{
    if (false)
    {
        printMessage("end totalMass");
    }

//{{{ begin code
    
//}}} end code

    return true;
}


// ************************************************************************* //

