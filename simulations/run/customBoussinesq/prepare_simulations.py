from os import mkdir, getcwd, chdir
from os.path import join, relpath, dirname, basename
from typing import Iterable

from configparser import ConfigParser
from itertools import product
from dataclasses import dataclass
from argparse import ArgumentParser
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
import subprocess
import shutil


@dataclass
class VariedParameter:
	name: str
	values: list

def write_BC(val ,path):
    f=ParsedParameterFile(r"./{}/0/xCO2".format(path))
    for b in f["boundaryField"]:
        if "topWall" in b:
            f["boundaryField"][b]["value"]=r"uniform {}".format(str(val))
            f["boundaryField"][b]["type"]="fixedValue"
    f.writeFile()

def write_transport_properties(prop_dict,path):
	f=ParsedParameterFile(r"./{}/constant/transportProperties".format(path))
	f["nu"]=prop_dict["viscosity"]/prop_dict["rho"]
	f["D_CO2"]=prop_dict["D"]
	f.writeFile()

def write_block_mesh(a,mesh_dict,path):
	f=ParsedParameterFile(r"./{}/system/blockMeshDict".format(path))
	vertices=["(0 0 0)", "(b 0 0)", "(b a 0)", "(0 a 0)", "(0 0 h)","(b 0 h)","(b a h)","(0 a h)"]
	vertices = [s.replace("b", "0.2") for s in vertices]
	vertices = [s.replace("a", str(a)) for s in vertices]
	vertices = [s.replace("h", "0.4") for s in vertices]
	f["vertices"]=vertices
	f["blocks"]=['hex '+ "(0 1 2 3 4 5 6 7) "+ mesh_dict["cells"]+" "+ 'simpleGrading '+ '(1 1 1)']
	f.writeFile()

if __name__ == "__main__":

    apertures={1e-2:{"cells":"(400 20 800)","cores":128},1e-1:{"cells":"(200 100 400)","cores":128}}
    concentrations=(8e-6,1e-5,2e-5,1e-4)
    aq_properties={8:{"rho":999.85,"viscosity":1.39e-3,"D":1.18e-9},20:{"rho":998.20,"viscosity":1.00e-3,"D":1.60e-9}}
    work_dir=getcwd()
    all_combinations = list(product(apertures.keys(),aq_properties.keys(),concentrations))
    i=1
    for a in apertures.keys():
        nC=apertures[a]["cores"]
        for T in aq_properties.keys():
            a_str=str(a).replace(".","_")
            with open(f"run{i}_{nC}_fracture_{a_str}_{T}.sh", "w") as bash_script:
                bash_script.write("#!/bin/bash\n\n")
                bash_script.write(f"#SBATCH -n{nC} -J fracture_{a}_{T} \n")
                bash_script.write("#SBATCH --error=slurm-%j.err \n\n")
                for x in concentrations:
                    case_dir=r"Fracture_study_{a}_{T}_{x}".format(a=a,T=T,x=x)
                    case_dir=case_dir.replace(".","_")
                    shutil.copytree("fracture_template",case_dir)
                    write_BC(x,case_dir)
                    write_transport_properties(aq_properties[T],case_dir)
                    write_block_mesh(a,apertures[a],case_dir)
                    bash_script.write(f"cd {case_dir} && ./Allrun {nC} && cd .. \n")
                bash_script.write("done;\n")
                i=i+1

