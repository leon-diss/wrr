#!/bin/bash

#SBATCH -n128 -J fracture_0.1_20 
#SBATCH --error=slurm-%j.err 

cd Fracture_study_0_1_20_8e-06 && ./Allrun 128 && cd .. 
cd Fracture_study_0_1_20_1e-05 && ./Allrun 128 && cd .. 
cd Fracture_study_0_1_20_2e-05 && ./Allrun 128 && cd .. 
cd Fracture_study_0_1_20_0_0001 && ./Allrun 128 && cd .. 
done;
