#!/bin/bash

#SBATCH -n128 -J fracture_0.01_8 
#SBATCH --error=slurm-%j.err 

cd Fracture_study_0_01_8_8e-06 && ./Allrun 128 && cd .. 
cd Fracture_study_0_01_8_1e-05 && ./Allrun 128 && cd .. 
cd Fracture_study_0_01_8_2e-05 && ./Allrun 128 && cd .. 
cd Fracture_study_0_01_8_0_0001 && ./Allrun 128 && cd .. 
done;
